using System;
using Vintagestory.API.Client;
using Vintagestory.API.Common;
using Vintagestory.API.Config;
using Vintagestory.API.MathTools;

namespace Th3Dilli
{
  public class GuiDialogPaperPress : GuiDialogBlockEntity
  {
    EnumPosFlag screenPos;

    ElementBounds inputSlotBounds;

    protected override double FloatyDialogPosition => 0.6;

    protected override double FloatyDialogAlign => 0.8;

    public GuiDialogPaperPress(string dialogTitle, InventoryBase inventory, BlockPos blockEntityPos, ICoreClientAPI capi) : base(dialogTitle, inventory, blockEntityPos, capi)
    {
      if (IsDuplicate) return;
    }

    void SetupDialog()
    {
      ElementBounds pressBoundsLeft = ElementBounds.Fixed(0, 30, 150, 150);
      ElementBounds pressBoundsRight = ElementBounds.Fixed(170, 30, 150, 150);

      inputSlotBounds = ElementStdBounds.SlotGrid(EnumDialogArea.None, 0, 30, 1, 1);
      inputSlotBounds.fixedHeight += 10;

      double top = inputSlotBounds.fixedHeight + inputSlotBounds.fixedY;

      ElementBounds bgBounds = ElementBounds.Fill.WithFixedPadding(GuiStyle.ElementToDialogPadding);
      bgBounds.BothSizing = ElementSizing.FitToChildren;
      bgBounds.WithChildren(pressBoundsLeft, pressBoundsRight);

      ElementBounds dialogBounds = ElementStdBounds.AutosizedMainDialog
          .WithFixedAlignmentOffset(IsRight(screenPos) ? -GuiStyle.DialogToScreenPadding : GuiStyle.DialogToScreenPadding, 0)
          .WithAlignment(IsRight(screenPos) ? EnumDialogArea.RightMiddle : EnumDialogArea.LeftMiddle);

      SingleComposer = capi.Gui
          .CreateCompo("blockentitypress" + BlockEntityPosition, dialogBounds)
          .AddShadedDialogBG(bgBounds)
          .AddDialogTitleBar(DialogTitle, OnTitleBarClose)
          .BeginChildElements(bgBounds)
          .AddItemSlotGrid(Inventory, SendInvPacket, 1, new int[] { 0 }, inputSlotBounds, "inputSlot")
          .AddSmallButton("press", OnPressClick, ElementBounds.Fixed(0, 100, 80, 25), EnumButtonStyle.Normal)
          .AddDynamicText(GetContentsText(), CairoFont.WhiteDetailText(), pressBoundsRight, "contentText")
          .EndChildElements()
          .Compose();
    }

    private void OnTitleBarClose()
    {
      TryClose();
    }

    private void SendInvPacket(object packet)
    {
      capi.Network.SendBlockEntityPacket(BlockEntityPosition.X, BlockEntityPosition.Y, BlockEntityPosition.Z, packet);
    }

    private bool OnPressClick()
    {
      BlockEntityPaperPress bepaperpress = capi.World.BlockAccessor.GetBlockEntity(BlockEntityPosition) as BlockEntityPaperPress;
      if (bepaperpress == null || bepaperpress.Pressing)
      {
        return true;
      }

      if (bepaperpress.CurrentRecipeName == string.Empty)
      {
        return true;
      }

      bepaperpress.PressPaper();

      capi.Network.SendBlockEntityPacket(BlockEntityPosition.X, BlockEntityPosition.Y, BlockEntityPosition.Z, 1337);
      Vec3d pos = BlockEntityPosition.ToVec3d().Add(0.5, 0.5, 0.5);
      capi.World.PlaySoundAt(new AssetLocation("sounds/player/seal"), pos.X, pos.Y, pos.Z, null);

      TryClose();
      return true;
    }

    public override void OnGuiOpened()
    {
      base.OnGuiOpened();
      screenPos = GetFreePos("smallblockgui");
      OccupyPos("smallblockgui", screenPos);
      SetupDialog();
    }

    public override void OnGuiClosed()
    {
      SingleComposer.GetSlotGrid("inputSlot").OnGuiClosed(capi);

      base.OnGuiClosed();

      FreePos("smallblockgui", screenPos);
    }

    string GetContentsText()
    {
      string contents = "Contents:";

      if (Inventory[0].Empty)
      {
        contents += "\nNone.";
      }
      else
      {
        if (!Inventory[0].Empty)
        {
          ItemStack stack = Inventory[0].Itemstack;
          contents += "\n" + Lang.Get("{0}x of {1}", stack.StackSize, stack.GetName());
        }

        BlockEntityPaperPress bebarrel = capi.World.BlockAccessor.GetBlockEntity(BlockEntityPosition) as BlockEntityPaperPress;
        if (bebarrel.CurrentRecipeName != string.Empty)
        {
          string timeText = bebarrel.CurrentRecipePressHours > 24 ? Lang.Get("{0} days", Math.Round(bebarrel.CurrentRecipePressHours / capi.World.Calendar.HoursPerDay, 1)) : Lang.Get("{0} hours", bebarrel.CurrentRecipePressHours);

          contents += "\n\n" + Lang.Get("Will turn into {0}x {1} after {2} of pressing.", bebarrel.CurrentOutSize, bebarrel.CurrentRecipeName, timeText);
        }
      }
      return contents;
    }

    public void UpdateContents()
    {
      SingleComposer.GetDynamicText("contentText").SetNewText(GetContentsText());
    }
  }
}