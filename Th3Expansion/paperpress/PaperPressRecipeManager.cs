using System.Collections.Generic;
using Vintagestory.API.Common;
using Vintagestory.API.Config;
using Vintagestory.API.Server;
using Vintagestory.ServerMods;

namespace Th3Dilli
{
    public class PaperPressRecipeManager : ModSystem
    {
        public static List<PaperPressRecipe> PaperPressRecipes;

        private ICoreServerAPI api;

        public override void StartServerSide(ICoreServerAPI api)
        {
            this.api = api;
            api.Event.SaveGameLoaded += OnSaveGameLoaded;
        }

        public void OnSaveGameLoaded()
        {
            PaperPressRecipes = new List<PaperPressRecipe>();
            RecipeLoader recipeLoader = api.ModLoader.GetModSystem<RecipeLoader>();
            recipeLoader.LoadRecipes<PaperPressRecipe>("paper press recipe", "recipes/paperpress", (r) => PaperPressRecipes.Add(r));
            api.World.Logger.StoryEvent(Lang.Get("th3expansion:recipeloading"));
        }
    }
}