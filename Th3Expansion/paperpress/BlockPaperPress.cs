using System;
using System.Collections.Generic;
using System.Linq;
using Vintagestory.API.Client;
using Vintagestory.API.Common;
using Vintagestory.API.Config;
using Vintagestory.API.MathTools;
using Vintagestory.GameContent;

namespace Th3Dilli
{
  public class BlockPaperPress : BlockContainer
  {

    public BlockFacing Orientation => BlockFacing.FromCode(Variant["side"]);

    public override bool DoPlaceBlock(IWorldAccessor world, IPlayer byPlayer, BlockSelection blockSel, ItemStack byItemStack)
    {
      BlockPos pos = blockSel.Position;
      Block blockToPlace = api.World.GetBlock(CodeWithVariant("side", Orientation.Code));
      world.BlockAccessor.SetBlock(blockToPlace.BlockId, pos);
      return true;
    }

    public override bool OnBlockInteractStart(IWorldAccessor world, IPlayer byPlayer, BlockSelection blockSel)
    {
      BlockEntityPaperPress bepress = null;
      if (blockSel.Position != null)
      {
        bepress = world.BlockAccessor.GetBlockEntity(blockSel.Position) as BlockEntityPaperPress;
      }
      bool handled = base.OnBlockInteractStart(world, byPlayer, blockSel);

      if (!handled && !byPlayer.WorldData.EntityControls.Sneak && blockSel.Position != null)
      {
        if (bepress != null)
        {
          bepress.OnBlockInteract(byPlayer);
        }

        return true;
      }

      return handled;
    }

    public override string GetPlacedBlockInfo(IWorldAccessor world, BlockPos pos, IPlayer forPlayer)
    {
      string text = base.GetPlacedBlockInfo(world, pos, forPlayer);
      if (world.BlockAccessor.GetBlockEntity(pos) is BlockEntityPaperPress bebarrel)
      {
        ItemSlot slot = bebarrel.Inventory[0];
        if (!slot.Empty)
        {
          if (text.Length > 0)
          {
            text += " ";
          }
          else
          {
            text += Lang.Get("Contents:") + "\n";
          }

          text += Lang.Get("{0}x {1}", slot.Itemstack.StackSize, slot.Itemstack.GetName());
        }

        if (bebarrel.Pressing && bebarrel.CurrentRecipeName != string.Empty)
        {
          double hoursPassed = world.Calendar.TotalHours - bebarrel.PressingSinceTotalHours;
          string timePassedText = hoursPassed > 24 ? Lang.Get("{0} days", Math.Round(hoursPassed / api.World.Calendar.HoursPerDay, 1)) : Lang.Get("{0} hours", Math.Round(hoursPassed));
          string timeTotalText = bebarrel.CurrentRecipePressHours > 24 ? Lang.Get("{0} days", Math.Round(bebarrel.CurrentRecipePressHours / api.World.Calendar.HoursPerDay, 1)) : Lang.Get("{0} hours", Math.Round(bebarrel.CurrentRecipePressHours));
          text += "\n" + Lang.Get("Sealed for {0} / {1}", timePassedText, timeTotalText);
        }
      }
      return text;
    }

    public MeshData GenMesh(bool isPressing)
    {
      ICoreClientAPI capi = api as ICoreClientAPI;

      Shape shape = capi.Assets.TryGet("th3expansion:shapes/block/paperpress/" + (isPressing ? "closed" : "open") + ".json").ToObject<Shape>();
      MeshData barrelMesh;
      Console.WriteLine(Shape.rotateY.ToString());
      capi.Tesselator.TesselateShape(this, shape, out barrelMesh, new Vec3f(Shape.rotateX, Shape.rotateY, Shape.rotateZ));
      return barrelMesh;
    }

    public override void OnBeforeRender(ICoreClientAPI capi, ItemStack itemstack, EnumItemRenderTarget target, ref ItemRenderInfo renderinfo)
    {
      Dictionary<int, MultiTextureMeshRef> meshrefs;

      object obj;
      if (capi.ObjectCache.TryGetValue("paperpresslMeshRefs", out obj))
      {
        meshrefs = obj as Dictionary<int, MultiTextureMeshRef>;
      }
      else
      {
        capi.ObjectCache["paperpresslMeshRefs"] = meshrefs = new Dictionary<int, MultiTextureMeshRef>();
      }

      ItemStack[] contentStacks = GetContents(capi.World, itemstack);
      if (contentStacks == null || contentStacks.Length == 0) return;

      bool isPressing = itemstack.Attributes.GetBool("pressing");

      int hashcode = GetPaperPressHashCode(capi.World, contentStacks[0]);

      if (!meshrefs.TryGetValue(hashcode, out var meshRef))
      {
        MeshData meshdata = GenMesh(isPressing);
        meshrefs[hashcode] = meshRef = capi.Render.UploadMultiTextureMesh(meshdata);
      }

      renderinfo.ModelRef = meshRef;
    }

    public int GetPaperPressHashCode(IClientWorldAccessor world, ItemStack contentStack)
    {
      string s = contentStack?.StackSize + "x" + contentStack?.Collectible.Code.ToShortString();
      return s.GetHashCode();
    }
  }
}