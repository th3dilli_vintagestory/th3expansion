using Vintagestory.API.Client;
using Vintagestory.API.Common;
using Vintagestory.API.Datastructures;
using Vintagestory.GameContent;

namespace Th3Dilli
{
  public class BlockEntityPaperPress : BlockEntityContainer
  {
    GuiDialogPaperPress invDialog;

    internal InventoryGeneric inventory;

    public override InventoryBase Inventory => inventory;

    public override string InventoryClassName => "press";

    public PaperPressRecipe CurrentRecipe;

    public string CurrentRecipeName;

    public double CurrentRecipePressHours;

    public int CurrentOutSize;

    public bool Pressing;

    public double PressingSinceTotalHours;

    MeshData currentMesh;

    BlockPaperPress ownBlock;

    public BlockEntityPaperPress()
    {
      inventory = new InventoryGeneric(1, null, null, (id, self) =>
      {
        return new ItemSlotInput(self, id);
      })
      {
        BaseWeight = 1
      };

      inventory.SlotModified += SlotModified;
      CurrentRecipePressHours = -1;
      CurrentOutSize = -1;
      PressingSinceTotalHours = -1;
    }

    public void SlotModified(int slotId)
    {
      if (Api?.Side == EnumAppSide.Client)
      {
        invDialog?.UpdateContents();
        currentMesh = GenMesh();
        MarkDirty(true);
      }

      if (Api?.Side == EnumAppSide.Server)
      {
        FindMatchingRecipe();
        MarkDirty(true);
      }
    }

    public override void Initialize(ICoreAPI api)
    {
      base.Initialize(api);

      ownBlock = Block as BlockPaperPress;

      if (api.Side == EnumAppSide.Client && currentMesh == null)
      {
        currentMesh = GenMesh();
        MarkDirty(true);
      }
      if (api.Side == EnumAppSide.Server)
      {
        FindMatchingRecipe();
        MarkDirty(true);
        RegisterGameTickListener(OnEvery3Second, 3000);
        inventory.OnAcquireTransitionSpeed += Inventory_OnAcquireTransitionSpeed;
      }
    }

    private MeshData GenMesh()
    {
      if (ownBlock == null) return null;
      return ownBlock.GenMesh(Pressing);
    }

    private void OnEvery3Second(float dt)
    {
      if (!inventory[0].Empty && CurrentRecipe == null)
      {
        FindMatchingRecipe();
      }

      if (CurrentRecipe != null)
      {
        if (Pressing && CurrentRecipe.TryCraftNow(Api, Api.World.Calendar.TotalHours - PressingSinceTotalHours, new ItemSlot[] { inventory[0] }) == true)
        {
          Pressing = false;
          Api.World.BlockAccessor.MarkBlockEntityDirty(Pos);
          MarkDirty(true);
        }
      }
      else
      {
        if (Pressing)
        {
          Pressing = false;
          MarkDirty(true);
        }
      }
    }

    public override void OnBlockRemoved()
    {
      if (!Pressing)
      {
        base.OnBlockBroken();
      }
      if (Api.Side == EnumAppSide.Client)
      {
        invDialog?.TryClose();
        invDialog = null;
      }
    }

    public override void OnReceivedClientPacket(IPlayer fromPlayer, int packetid, byte[] data)
    {
      if (packetid <= (int)EnumBlockEntityPacketId.Open)
      {
        inventory.InvNetworkUtil.HandleClientPacket(fromPlayer, packetid, data);
      }

      if (packetid == (int)EnumBlockEntityPacketId.Close)
      {
        if (fromPlayer.InventoryManager != null)
        {
          fromPlayer.InventoryManager.CloseInventory(Inventory);
        }
      }

      if (packetid == 1337)
      {
        PressPaper();
      }
    }

    internal void OnBlockInteract(IPlayer byPlayer)
    {
      if (Pressing) { return; }
      if (Api.Side == EnumAppSide.Client)
      {
        if (invDialog == null)
        {
          invDialog = new GuiDialogPaperPress("Paper Press", Inventory, Pos, Api as ICoreClientAPI);
          invDialog.OnClosed += () =>
          {
            invDialog = null;
            (Api as ICoreClientAPI).Network.SendBlockEntityPacket(Pos.X, Pos.Y, Pos.Z, (int)EnumBlockEntityPacketId.Close, null);
            byPlayer.InventoryManager.CloseInventory(inventory);
          };
        }

        invDialog.TryOpen();

        (Api as ICoreClientAPI).Network.SendPacketClient(inventory.Open(byPlayer));
      }
    }

    internal void PressPaper()
    {
      if (Pressing)
      {
        return;
      }
      Pressing = true;
      PressingSinceTotalHours = Api.World.Calendar.TotalHours;
      MarkDirty(true);
    }

    public override void FromTreeAttributes(ITreeAttribute tree, IWorldAccessor worldForResolving)
    {
      base.FromTreeAttributes(tree, worldForResolving);

      Pressing = tree.GetBool("pressing");
      PressingSinceTotalHours = tree.GetDouble("pressingSinceTotalHours");
      CurrentRecipeName = tree.GetString("CurrentRecipeName");
      CurrentRecipePressHours = tree.GetDouble("CurrentRecipePressHours");
      CurrentOutSize = tree.GetInt("CurrentOutSize");

      if (Api?.Side == EnumAppSide.Client)
      {
        invDialog?.UpdateContents();
        currentMesh = GenMesh();
        MarkDirty(true);
      }

      if (Api?.Side == EnumAppSide.Server)
      {
        FindMatchingRecipe();
      }
    }

    public override void ToTreeAttributes(ITreeAttribute tree)
    {
      base.ToTreeAttributes(tree);

      tree.SetBool("pressing", Pressing);
      tree.SetDouble("pressingSinceTotalHours", PressingSinceTotalHours);
      tree.SetString("CurrentRecipeName", CurrentRecipeName);
      tree.SetDouble("CurrentRecipePressHours", CurrentRecipePressHours);
      tree.SetInt("CurrentOutSize", CurrentOutSize);
    }

    protected float Inventory_OnAcquireTransitionSpeed(EnumTransitionType transType, ItemStack stack, float baseMul)
    {
      // Don't spoil while sealed
      if (Pressing && CurrentRecipeName != string.Empty && CurrentRecipe.PressHours > 0)
      {
        return 0;
      }

      return baseMul;
    }

    private void FindMatchingRecipe()
    {
      ItemSlot[] inputSlots = new ItemSlot[] { inventory[0] };
      CurrentRecipe = null;
      CurrentOutSize = -1;
      CurrentRecipeName = string.Empty;
      CurrentRecipePressHours = -1;

      foreach (PaperPressRecipe recipe in PaperPressRecipeManager.PaperPressRecipes)
      {
        int outsize;

        if (recipe.Matches(inputSlots, out outsize))
        {
          CurrentRecipe = recipe;
          CurrentOutSize = outsize;
          CurrentRecipeName = recipe.Output.ResolvedItemstack.GetName();
          CurrentRecipePressHours = recipe.PressHours;
          MarkDirty(true);
          return;
        }
      }
    }

    public override void OnBlockUnloaded()
    {
      base.OnBlockUnloaded();

      invDialog?.Dispose();
    }

    public override bool OnTesselation(ITerrainMeshPool mesher, ITesselatorAPI tesselator)
    {
      mesher.AddMeshData(currentMesh);
      return true;
    }
  }
}