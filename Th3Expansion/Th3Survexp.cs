using Vintagestory.API.Common;

[assembly: ModInfo("th3expansion",
    Description = "th3expansion",
    Website = "https://github.com/Th3Dilli/",
    Authors = new[] { "Th3Dilli" })]
namespace Th3Dilli
{
  public class SurvivalExpansion : ModSystem
  {

    public override void Start(ICoreAPI api)
    {
      base.Start(api);
      api.RegisterBlockClass("BlockPaperPress", typeof(BlockPaperPress));
      api.RegisterBlockEntityClass("BEPaperPress", typeof(BlockEntityPaperPress));
    }
  }
}
